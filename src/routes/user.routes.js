import { Router } from 'express'
import { getUsers, createUser, login, logout, profile } from '../controllers/user.controller.js'
import { authRequired } from '../middlewares/validateToken.js'

const router = Router()

router.get('/users', getUsers)
router.post('/users/login', login)
router.post('/users/register', createUser)
router.post('/users/logout', logout)
router.get('/users/profile', authRequired, profile)


export default router