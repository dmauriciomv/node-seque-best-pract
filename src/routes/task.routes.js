import { authRequired } from '../middlewares/validateToken.js'
import { getTasks, createTask, getTaskById, deleteTask, updateTask, getTaskByIdAndUserId, getTasksByUserId } from '../controllers/task.controller.js'
import { Router } from 'express'

const router = Router()

router.get('/tasks', authRequired, getTasks)
router.get('/tasks/:id', authRequired, getTaskById)
router.get('/tasks/:userId', authRequired, getTasksByUserId)
router.get('/user/:userId/task/:taskId', authRequired, getTaskByIdAndUserId)

router.post('/tasks/register', authRequired, createTask)
router.put('/tasks/:id', authRequired, updateTask)
router.delete('/tasks/:id', authRequired, deleteTask)

export default router