import { DataTypes } from 'sequelize'
import { sequelize } from '../database/database.js'
import { Task } from './Task.js'

export const User = sequelize.define('user', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    username: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: 'compositeIndex'
    },
    password: {
        type: DataTypes.STRING,
        allowNull: false,
    },
}, { timestamp: true })

User.hasMany(Task, {
    foreignKey: 'userId',
    sourceKey: 'id'
})

Task.belongsTo(User, {
    foreignKey: { name: 'userId', allowNull: false },
    targetKey: 'id'
})