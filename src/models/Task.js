import { DataTypes } from 'sequelize'
import { sequelize } from '../database/database.js'

export const Task = sequelize.define('task', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    description: {
        type: DataTypes.STRING,
        allowNull: false,
    },
}, { timestamp: true })