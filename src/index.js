import app from './app.js'
import { sequelize } from './database/database.js'
import './models/User.js'

const PORT = 3000

async function main() {
    try {
        await sequelize.sync()
        console.log("Succesfully connected on port, ", PORT)
        app.listen(PORT)
    } catch (error) {
        console.log("Error trying to connect: ", error)
    }
}

main()
