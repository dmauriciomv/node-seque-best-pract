import { Task } from "../models/Task.js"
import { User } from "../models/User.js"

export const getTasks = async (req, res) => {
    try {
        const tasks = await Task.findAll({
            include: [{
              model: User,
              attributes: ['id', 'email'],
            }],
          })
        res.json(tasks)
    } catch (error) {
        return res.status(500).json({ message: error.message })
    }
}

export const createTask = async (req, res) => {
    try {
        const { description } = req.body

        const newTask = await Task.create({ description, userId: req.user.id })
        res.json(newTask)
    } catch (error) {
        return res.status(500).json({ message: error.message })
    }
}

export const updateTask = async (req, res) => {
    try {
        const { id } = req.params
        const { description } = req.body
        const taskToUpdate = await Task.findByPk(id)

        taskToUpdate.description = description

        await taskToUpdate.save()

        res.json(taskToUpdate)
    } catch (error) {
        return res.status(500).json({ message: error.message })
    }
}

export const deleteTask = async (req, res) => {
    try {
        const { id } = req.params
        await Task.destroy({
            where: { id }
        })
        return res.sendStatus(200)
    } catch (error) {
        return res.status(500).json({ message: error.message })
    }
}

export const getTaskById = async (req, res) => {
    const { id } = req.params
    try {
        const taskFound = await Task.findByPk(id)
        res.json(taskFound)
    } catch (error) {
        return res.status(500).json({ message: error.message })
    }
}

export const getTasksByUserId = async (req, res) => {
    try {
        const { userId } = req.params
        const tasks = await User.findAll({
            where: { userId: userId }
        })

        res.json(tasks)
    } catch (error) {
        return res.status(500).json({ message: error.message })
    }
}

export const getTaskByIdAndUserId = async (req, res) => {
    try {
        const { userId, taskId } = req.params
        const task = await Task.findOne({
            where: { id: taskId, userId: userId }
        })
        res.json(task)
    } catch (error) {
        return res.status(500).json({ message: error.message })
    }
}