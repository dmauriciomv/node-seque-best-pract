import { createAccessToken } from "../libs/jwt.js"
import { User } from "../models/User.js"
import bcrypt from 'bcryptjs'


export const getUsers = async (req, res) => {
    try {
        const users = await User.findAll()
        res.json(users)
    } catch (error) {
        return res.status(500).json({ message: error.message })
    }
}

export const createUser = async (req, res) => {
    const { username, email, password } = req.body

    try {
        const passwordHash = await bcrypt.hash(password, 10)

        const newUser = await User.create({
            username, email, password: passwordHash,
        })

        const token = await createAccessToken({ id: newUser.id })

        res.cookie("token", token)
        res.json({
            id: newUser.id,
            email: newUser.email,
            username: newUser.username
        })

    } catch (error) {
        return res.status(500).json({ message: error.message })
    }
}

export const login = async (req, res) => {
    const { email, password } = req.body
    try {
        const userFound = await User.findOne({ where: { email: email } })

        if (!userFound) return res.status(400).json({ message: "User not found." })

        const isMatch = await bcrypt.compare(password, userFound.password)

        if (!isMatch) { return res.status(400).json({ message: "Incorrect password." }) }

        const token = await createAccessToken({ id: userFound.id })

        res.cookie("token", token)

        res.json({
            username: userFound.username,
            email: userFound.email
        })
    } catch (error) {
        return res.status(500).json({ message: error.message })
    }
}

export const logout = (req, res) => {
    res.cookie("token","", {
        expires: new Date(0)
    })
    return res.sendStatus(200)
}

export const profile = async (req, res) => {
    const userFound = await User.findByPk(req.user.id)
    if (!userFound) return res.status(400).json({ message: "User not found." })
    return res.json({
        id: userFound.id,
        email: userFound.email,
        username: userFound.username
    })
}
